#from django.conf.urls import *
from django.conf.urls import url, include, static
from django.conf import settings
from gallery.feeds import Photos, Videos, Tags, TagContents, Comments
from gallery import views
from django.contrib.staticfiles import views as static_views

try:
    import django_openidconsumer
except ImportError:
    django_openidconsumer = None

urlpatterns = [
    url(r'^$', views.index),
    url(r'^feeds/comments/$', Comments()),
    url(r'^feeds/photos/$', Photos()),
    url(r'^feeds/videos/$', Videos()),
    url(r'^feeds/tags/$', Tags()),
    url(r'^feeds/tag/(?P<tag_id>[\w\-]*)$', TagContents()),
    url(r'^tag/(?P<tag_name>[\w\+\-]*)/$', views.medias_in_tag),
    url(r'^tag/(?P<tag_name>[\w\+\-]*)/(?P<page>\d+)/$',
        views.medias_in_tag),
    url(r'^event/(?P<media_type>\w+)/(?P<media_id>\d+)/(?P<event_id>\d+)/$',
        views.medias_in_event),
    url(r'^photo/(?P<photo_id>\d+)/(?P<tag_name>[\w\-]*)/$',
        views.medias_in_tag),
    url(r'^photo/(?P<photo_id>\d+)/$', views.photo),
    url(r'^video/(?P<video_id>\d+)/(?P<tag_name>[\w\-]*)/$',
        views.medias_in_tag),
    url(r'^date/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/$',
        views.date),
    url(r'^date/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/(?P<page>\d+)/$',
        views.date),
    url(r'^recent/$', views.recent),
    url(r'^recent/(?P<tag_name>[\w\+\-]*)/$', views.recent),
    url(r'^recent/(?P<tag_name>[\w\+\-]*)/(?P<page>\d+)/$', views.recent),
    url(r'^events/$', views.events),
    url(r'^event/(?P<event_id>\d+)/$', views.event),
    url(r'^slideshow/(?P<tag_name>[\w\+\-]*)/(?P<photo_id>\d+)/$', views.slideshow),
]

if django_openidconsumer:
    urlpatterns.extend([
        #(r'^comment/(?P<comment_id>\d+)/$', 'gallery.views.comment'),
        url(r'^openid/$', include('django_openidconsumer.views.begin'), {'sreg': 'fullname'}),
        url(r'^openid/complete/$', include('django_openidconsumer.views.complete')),
        url(r'^openid/signout/$', include('django_openidconsumer.views.signout')),
        url(r'^status/cache/$', include('gallery.memcached_status.cache_status')),
    ])

media_path = settings.GALLERY_SETTINGS.get('media_path')
static_path = settings.GALLERY_SETTINGS.get('static_path')

if media_path:
    urlpatterns.extend(static.static("/media/", document_root=media_path))

if static_path:
    urlpatterns.extend(static.static(settings.STATIC_URL, document_root=static_path))
