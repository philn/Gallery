from django.contrib.syndication.views import Feed
from gallery.models import Tag, Photo, Video, Comment
from django.core.exceptions import ObjectDoesNotExist

class Photos(Feed):
    title = "Photos recently added"
    link = "/gallery/"
    description = title
    description_template = "feeds/photos_description.html"

    def items(self):
        return Photo.objects.using("gallery").order_by('-timestamp')[:15]

class Videos(Feed):
    title = "Videos recently added"
    link = "/gallery/"
    description = title
    description_template = "feeds/videos_description.html"

    def items(self):
        return Video.objects.using("gallery").order_by('-time_created')[:15]

class Tags(Feed):
    title = "Tags recently updated"
    link = "/gallery/"
    description = "Tags recently updated"
    description_template = "feeds/tags_description.html"

    def items(self):
        return Tag.get_recent_tags_cloud()

    def item_title(self, obj):
        return obj.human_name

class TagContents(Feed):
    description_template = "feeds/photos_description.html"

    def get_object(self, request, tag_id):
        return Tag.with_name(tag_id)

    def title(self, obj):
        return "Medias tagged with %s" % obj.name

    def link(self, obj):
        return obj.get_absolute_url()

    def items(self, obj):
        photos = obj.photo_set.order_by('-timestamp')[:15]
        videos = obj.video_set.order_by('-time_created')[:15]
        medias = list(photos) + list(videos)
        medias.sort()
        medias.reverse()
        return medias[:15]

    def item_title(self, obj):
        return obj.human_name

class Comments(Feed):
    title = "Recent comments"
    link = "/gallery/"
    description = "Comments recently added"
    description_template = "feeds/comments_description.html"
    title_template = "feeds/comments_title.html"

    def items(self):
        return Comment.objects.filter(public=True).order_by('-submit_date')[:15]
