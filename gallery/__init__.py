import settings as gal_settings
from django.core import exceptions
from django.conf import settings

try:
    settings.GALLERY_SETTINGS = gal_settings.GALLERY_SETTINGS
except exceptions.ImproperlyConfigured:
    pass
