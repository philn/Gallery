
How to install this ?
=====================

1. Install Django :)
2. Start a new project and configure it:

   ::

     $ django-admin.py startproject mysite
     $ cd mysite
     $ # add 'django.contrib.sites' to mysite/settings.py INSTALLED_APPS
     $ python manage.py syncdb
     $ python manage.py runserver
     $ # from the admin site add a new Site in the db

3. Now you have a Django site, you can install the Gallery application

   ::

     $ cd ..
     $ mv gallery mysite/mysite/
     $ cd mysite/
     $ # add 'mysite.gallery' to mysite/setting.py INSTALLED_APPS
     $ # configure the gallery by editing gallery/settings.py,
     $ # register the 'gallery' db in DATABASES

   ::

     $ python manage.py sql gallery

   This outputs a bunch of SQL commands, you'll need to execute them
   in your site's sqlite db, but only the ones for <default>
   connection:

   ::

     $ sqlite3 site.db
     sqlite> # create table gallery_tagaddon gallery_photoaddon
               gallery_videoaddon and gallery_comment

5. Now you need to bind the gallery's urls to your django site:

   ::
   
     $ # edit site/urls.py and add this:

         url(r'^gallery/', include('mysite.gallery.urls')),

       # in urlpatterns variable

   This will bind all urls starting with gallery/ to the gallery
   application.


6. Start the testing server

   ::

     $ python manage.py runserver


7. Enjoy :)
