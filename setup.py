# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

data_files = []

readme = open('README.rst').read()
version = '0.1'

setup(name="django-gallery",
      version=version,
      description="A gallery app for Django",
      long_description=readme,
      author="Philippe Normand",
      author_email='phil@base-art.net',
      license="GPL2",
      packages=find_packages(),
      include_package_data=True,
      data_files=data_files,
      url="http://base-art.net",
      download_url='http://base-art.net/static/django-gallery-%s.tar.gz' % version,
      keywords=[],
      classifiers=['Development Status :: 5 - Production/Stable',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   ],
      entry_points="""\
      [console_scripts]
      gallery-sync-from-shotwell = gallery.sync:main
      """,
)
